
int main (string[] args) {
	var app = new Gtk.Application ("org.gnome.gitlab.alicem.MagicTrackpadConfig", ApplicationFlags.FLAGS_NONE);

    app.startup.connect (() => {
        Adw.init ();
    });

	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new MagicTrackpadConfig.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
