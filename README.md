# MTP2 Config

A very quick hack I've been using locally to configure MTP2 clicking.

Needs [an udev rule][1] to work.

```bash
cp data/10-mtp2-user-access.rules /etc/udev/rules.d
sudo systemd-hwdb update
```

- Only works with USB, not Bluetooth
- Doesn't check if an MTP2 is actually connected
- If multiple devices are connected, changes all at once
- Doesn't remember settings
- MTP2 only, despite the misleading name
- More importantly: click type only changes feedback but not pressure

The latter is a driver limitation - with click-on-device we can't change that,
it needs click-on-host. It could be made to work with [this driver][2], though
the app will need changes to work with it (but will get bluetooth support for
free).

It can still work with bluetooth, but you'll have to connect the touchpad via
USB, change settings and disconnect it. Similarly, a macOS VM can be used for
this.

[1]: https://gitlab.gnome.org/alicem/mtp2-config/-/blob/main/data/10-mtp2-user-access.rules
[2]: https://github.com/mwyborski/Linux-Magic-Trackpad-2-Driver
